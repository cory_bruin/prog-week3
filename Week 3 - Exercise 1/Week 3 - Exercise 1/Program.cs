﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week_3___Exercise_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var food = new string[5] { "Hotdog", "Chips", "Pie", "Burger", "Ice Cream" };

            for (var i = 0; i < food.Length; i++)
            {
                Console.WriteLine(food[i]);
            }
        }
    }
}
