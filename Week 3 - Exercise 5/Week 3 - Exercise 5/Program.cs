﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week_3___Exercise_5
{
    class Program
    {
        static void Main(string[] args)
        {
            var colour = new String[5] { "red", "blue", "orange", "pink", "purple" };
            Array.Reverse(colour);
            Console.WriteLine(string.Join(", ", colour));
        }
    }
}
