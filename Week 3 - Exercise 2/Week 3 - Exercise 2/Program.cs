﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week_3___Exercise_2
{
    class Program
    {
        static void Main(string[] args)
        {
            var colours = new string[5] {"Blue", "Red", "Yellow", "Green", "Orange"};

            foreach (var x in colours)
            {
                Console.WriteLine(x);
            }
        }
    }
}
