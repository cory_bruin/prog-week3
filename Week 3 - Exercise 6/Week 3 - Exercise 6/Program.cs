﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week_3___Exercise_6
{
    class Program
    {
        static void Main(string[] args)
        {
            var colours = new List<string> {"blue", "red", "yellow", "green", "orange"};
            colours.Add("purple");
            Console.WriteLine(string.Join(", ", colours));

        }
    }
}
