﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week_3___Exercise_3
{
    class Program
    {
        static void Main(string[] args)
        {
            var colours = new String[5] { "red", "blue", "green", "yellow", "orange" };
            Console.WriteLine(string.Join(", ", colours));
        }
    }
}
